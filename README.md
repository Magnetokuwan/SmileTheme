此渠道随缘同步、更新，若要使用最新版本，请前往 [GitHub](https://github.com/ouyangyanhuo/TinaTheme) 下载最新版本

<p align="center">
    <img src="https://tva4.sinaimg.cn/large/008aATBzgy1h4k9e3x97nj30qe0qetd8.jpg" width="200" height="200" alt="Haku">
</p>

<div align="center">

# Tina

_Haku，是 Tina 小姐最爱的喵喵，它在这里守卫着 Tina 小姐_

</div>

## TinaTheme

一个为typecho移植的主题，源自于[hugo-tania](https://github.com/WingLim/hugo-tania)主题，原本是Hugo的主题，被移植到Typecho上在其基础上进行了深度修改的 TinaTheme 或许是你在Typecho上最好的选择

<div align="center">
    
[![AdminMD](https://img.shields.io/badge/Magneto-TinaTheme-brightgreen)](https://fmcf.cc/technology/523/)
![Version](https://img.shields.io/badge/Version-2.1.0-criticale)
![Repo-size](https://img.shields.io/github/languages/code-size/ouyangyanhuo/TinaTheme)
![License](https://img.shields.io/github/license/ouyangyanhuo/TinaTheme)
![Stars](https://img.shields.io/github/stars/ouyangyanhuo/TinaTheme)
![Forks](https://img.shields.io/github/forks/ouyangyanhuo/TinaTheme)
    
</div>

原主题：[https://github.com/WingLim/hugo-tania](https://github.com/WingLim/hugo-tania)

## 最近版本更新日志
- 优化 CSS 性能
- 优化 页脚按钮 显示内容
- 优化 友链 页面的 链接 显示
- 优化 项目推荐 内容描述的字体显示
- 修复 友链 页面的 页脚 对齐Bug
- 修复 更新检测时检测不到新版本的Bug
- 修复 页脚按钮 在 hover 状态下显示遮挡的问题
- 更改 小屏模式 下 头部 的颜色，黑 → 白
- 调整 更新检测源

## 功能与特性

- 简洁风格

- 适配深色模式

- HTML 原生自响应式布局

- 超轻量

- 高度自定义

- 完善的后台设置系统

- HighLight.js

- Pjax

欢迎提 Issues 和 PRs，欢迎提出建议

## 使用

下载主题包并解压 ( 若是从 GitHub 或 Gitee 下载，请把解压出来的文件夹改名为 `TinaTheme` ) ，将文件夹上传至网站文件主题目录 ( `/usr/theme` ) 下，进入网站后台 - 控制台 - 外观 - 启用主题即可。

## 文档

TinaTheme 文档 : [https://tina.docs.fmcf.cc](https://tina.docs.fmcf.cc)

## Demo

主题效果预览

TinaTheme主题演示站：[https://tina.fmcf.cc](https://tina.fmcf.cc)

## 注意

TinaTheme 使用 [GPL V3.0](https://github.com/ouyangyanhuo/TinaTheme/blob/main/LICENSE) 协议开源，请遵守此协议进行二次开发等。

您必须在页脚保留 TinaTheme 主题的名称及其指向链接，否则请不要使用 TinaTheme 主题。

您可以删除页脚的作者版权信息，但是不能删除 TinaTheme 主题的名称及其指向链接。

## 下载渠道

1.GitHub [https://github.com/ouyangyanhuo/TinaTheme](https://github.com/ouyangyanhuo/TinaTheme)  适合国外（主仓库）

2.取消 Gitee 下载 因为它真的比 Shit 还烂，Gitee仓库会保留，但不会同步、更新。

## 预览图
![](https://tva4.sinaimg.cn/large/008aATBzly1h4lu8mys6rj340e256kjl.jpg)
![](https://tva4.sinaimg.cn/large/008aATBzly1h4lu8ls0w1j340e2561ky.jpg)
![](https://tva4.sinaimg.cn/large/008aATBzly1h4lu8ktiopj31x20ysgys.jpg)
![](https://tva4.sinaimg.cn/large/008aATBzly1h4lu8k558gj31x20ysqgq.jpg)
![](https://tva4.sinaimg.cn/large/008aATBzly1h4lu8l3r4mj31x20ys4ec.jpg)
![](https://tva4.sinaimg.cn/large/008aATBzly1h763e5sccdj34vc2jsu10.jpg)